package ru.vbotalov.topicservice.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.vbotalov.topicservice.services.WordsService;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static ru.vbotalov.topicservice.TestData.getTestWord;

public class WordsApiTest {

    private WordsApi api;

    private WordsService wordsService;

    @BeforeEach
    public void before() {
        wordsService = mock(WordsService.class);

        api = new WordsApi(wordsService);
    }

    @Test
    public void shouldGetRandomWordSuccess() {
        when(wordsService.getRandomWords()).thenReturn(
                Collections.singletonList(
                        getTestWord("1")
                )
        );

        var response = api.getRandomWords();

        assertTrue(response.isSuccess());
        assertEquals(1, response.getWords().size());
        assertEquals(0, response.getCorrectWord());
    }

}
