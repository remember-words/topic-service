package ru.vbotalov.topicservice.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static ru.vbotalov.topicservice.TestData.*;
import static ru.vbotalov.topicservice.services.WordsService.WORD_COUNT;

public class WordsServiceTest {

    private WordsService service;

    private WordsLoader loader;

    @BeforeEach
    public void before() {
        loader = mock(WordsLoader.class);
        when(loader.readWords()).thenReturn(
                Arrays.asList(
                        getTestWord("1"),
                        getTestWord("2"),
                        getTestWord("3"),
                        getTestWord("4"),
                        getTestWord("5"),
                        getTestWord("6")
                )
        );

        service = new WordsService(loader);
    }

    @Test
    public void shouldGetRandomWordsSuccess() {
        var words = service.getRandomWords();

        assertEquals(WORD_COUNT, words.size());
        assertEquals(WORD_ORIGINAL, words.get(0).getOriginal());
        assertEquals(WORD_TRANSLATED, words.get(0).getTranslated());
    }

}
