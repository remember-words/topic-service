package ru.vbotalov.topicservice.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.vbotalov.topicservice.config.AppProperties;
import ru.vbotalov.topicservice.remote.DiscoveryApi;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static ru.vbotalov.topicservice.TestData.*;

public class RegisterServiceTest {

    private RegisterService service;

    private DiscoveryApi discoveryApi;
    private AppProperties properties;

    @BeforeEach
    public void before() {
        discoveryApi = mock(DiscoveryApi.class);
        properties = new AppProperties();
        properties.setId(TOPIC_ID);
        properties.setName(TOPIC_NAME);
        properties.setUrl(TOPIC_SERVICE_URL);
        properties.setDiscoveryUrl(DISCOVERY_URL);

        service = new RegisterService(
                discoveryApi,
                properties
        );
    }

    @Test
    public void shouldRegisterSuccess() {
        service.register();

        verify(discoveryApi
        ).register(argThat(request ->
                request.getTopicId().equals(TOPIC_ID)
                        && request.getTopicName().equals(TOPIC_NAME)
                        && request.getServiceUrl().equals(TOPIC_SERVICE_URL)));
    }

}
