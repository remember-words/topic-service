package ru.vbotalov.topicservice;

import ru.vbotalov.topicservice.model.Word;

public final class TestData {

    public static final String TOPIC_ID = "topic-id";
    public static final String TOPIC_NAME = "topic-name";
    public static final String TOPIC_SERVICE_URL = "https://topic:8080";

    public static final String DISCOVERY_URL = "https://discovery:8080";

    public static final String WORD_ORIGINAL = "word-original";
    public static final String WORD_TRANSLATED = "word-translated";

    public static Word getTestWord(String wordId) {
        var word = new Word();
        word.setId(wordId);
        word.setOriginal(WORD_ORIGINAL);
        word.setTranslated(WORD_TRANSLATED);
        return word;
    }

}
