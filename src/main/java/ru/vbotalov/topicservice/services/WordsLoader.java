package ru.vbotalov.topicservice.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.vbotalov.topicservice.config.AppProperties;
import ru.vbotalov.topicservice.model.Word;
import ru.vbotalov.topicservice.model.WordList;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

@Service
public class WordsLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(WordsLoader.class);

    private final AppProperties properties;

    public WordsLoader(AppProperties properties) {
        this.properties = properties;
    }

    public List<Word> readWords() {
        try {
            var reader = new BufferedReader(new FileReader(properties.getWordsLocation()));
            var jsonLines = new StringBuilder();
            while (reader.ready()) {
                jsonLines.append(reader.readLine());
            }
            var wordList = new ObjectMapper().readValue(jsonLines.toString(), WordList.class);
            return wordList.getWords();
        } catch (Exception e) {
            LOGGER.error("Can't read words", e);
            throw new RuntimeException(e);
        }
    }

}
