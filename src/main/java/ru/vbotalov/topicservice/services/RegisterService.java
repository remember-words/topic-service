package ru.vbotalov.topicservice.services;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.vbotalov.topicservice.config.AppProperties;
import ru.vbotalov.topicservice.remote.DiscoveryApi;
import ru.vbotalov.topicservice.remote.requests.RegisterRequest;

@Service
public class RegisterService {

    private final DiscoveryApi discoveryApi;
    private final AppProperties properties;

    public RegisterService(DiscoveryApi discoveryApi, AppProperties properties) {
        this.discoveryApi = discoveryApi;
        this.properties = properties;
    }

    @Scheduled(fixedRate = 30)
    public void register() {
        var request = new RegisterRequest(properties.getId(), properties.getName(), properties.getUrl());
        discoveryApi.register(request);
    }

}
