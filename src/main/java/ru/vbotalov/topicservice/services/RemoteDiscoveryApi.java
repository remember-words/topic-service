package ru.vbotalov.topicservice.services;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.vbotalov.topicservice.config.AppProperties;
import ru.vbotalov.topicservice.remote.DiscoveryApi;
import ru.vbotalov.topicservice.remote.requests.RegisterRequest;
import ru.vbotalov.topicservice.remote.responses.RegisterResponse;

@Service
public class RemoteDiscoveryApi implements DiscoveryApi {

    private final RestTemplate restTemplate;
    private final AppProperties properties;

    public RemoteDiscoveryApi(AppProperties properties) {
        restTemplate = new RestTemplate();
        this.properties = properties;
    }

    @Override
    public RegisterResponse register(RegisterRequest request) {
        var response = restTemplate.postForEntity(properties.getDiscoveryUrl() + "/api/v1/discovery/register",
                request,
                RegisterResponse.class);
        return response.getBody();
    }
}
