package ru.vbotalov.topicservice.services;

import org.springframework.stereotype.Service;
import ru.vbotalov.topicservice.model.Word;

import java.util.ArrayList;
import java.util.List;

@Service
public class WordsService {

    public static final int WORD_COUNT = 5;

    private final List<Word> words;

    public WordsService(WordsLoader loader) {
        words = new ArrayList<>(loader.readWords());
    }

    public List<Word> getRandomWords() {
        List<String> addedWordIds = new ArrayList<>();
        List<Word> randomWords = new ArrayList<>();
        while (addedWordIds.size() < WORD_COUNT && addedWordIds.size() < words.size()) {
            var randomIndex = (int)(Math.random() * words.size());
            var word = words.get(randomIndex);
            if (!addedWordIds.contains(word.getId())) {
                addedWordIds.add(word.getId());
                randomWords.add(word);
            }
        }
        return randomWords;
    }

}
