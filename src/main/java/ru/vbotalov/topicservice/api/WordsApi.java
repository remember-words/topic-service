package ru.vbotalov.topicservice.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vbotalov.topicservice.api.responses.GetRandomResponse;
import ru.vbotalov.topicservice.services.WordsService;

@RestController
@RequestMapping(path = "/api/v1/words")
public class WordsApi {

    private final WordsService wordsService;

    public WordsApi(WordsService wordsService) {
        this.wordsService = wordsService;
    }

    @GetMapping(path = "/random-words")
    public GetRandomResponse getRandomWords() {
        var words = wordsService.getRandomWords();
        var correctWord = (int)(Math.random() * words.size());
        return new GetRandomResponse(words, correctWord);
    }
}
