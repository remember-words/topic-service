package ru.vbotalov.topicservice.api.responses;

import ru.vbotalov.topicservice.model.Word;

import java.util.List;

public class GetRandomResponse {

    private final boolean success;
    private final List<Word> words;
    private final int correctWord;

    public GetRandomResponse(List<Word> words, int correctWord) {
        this.success = true;
        this.words = words;
        this.correctWord = correctWord;
    }

    public boolean isSuccess() {
        return success;
    }

    public List<Word> getWords() {
        return words;
    }

    public int getCorrectWord() {
        return correctWord;
    }
}
