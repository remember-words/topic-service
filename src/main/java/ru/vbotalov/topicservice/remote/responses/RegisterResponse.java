package ru.vbotalov.topicservice.remote.responses;

import ru.vbotalov.topicservice.remote.data.Topic;

public class RegisterResponse {

    private boolean success;
    private String message;
    private Topic topic;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }
}
