package ru.vbotalov.topicservice.remote;

import org.springframework.web.bind.annotation.RequestBody;
import ru.vbotalov.topicservice.remote.requests.RegisterRequest;
import ru.vbotalov.topicservice.remote.responses.RegisterResponse;

public interface DiscoveryApi {

    RegisterResponse register(@RequestBody RegisterRequest request);

}
