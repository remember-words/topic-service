package ru.vbotalov.topicservice.remote.requests;

import org.apache.logging.log4j.message.StringFormattedMessage;

public class RegisterRequest {

    private final String topicId;
    private final String topicName;
    private final String serviceUrl;

    public RegisterRequest(String topicId, String topicName, String serviceUrl) {
        this.topicId = topicId;
        this.topicName = topicName;
        this.serviceUrl = serviceUrl;
    }

    public String getTopicId() {
        return topicId;
    }

    public String getTopicName() {
        return topicName;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }
}
