package ru.vbotalov.topicservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "app")
public class AppProperties {

    private String discoveryUrl;
    private String id;
    private String name;
    private String url;
    private String wordsLocation;

    public String getDiscoveryUrl() {
        return discoveryUrl;
    }

    public void setDiscoveryUrl(String discoveryUrl) {
        this.discoveryUrl = discoveryUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWordsLocation() {
        return wordsLocation;
    }

    public void setWordsLocation(String wordsLocation) {
        this.wordsLocation = wordsLocation;
    }
}
