package ru.vbotalov.topicservice.model;

import java.util.List;

public class WordList {

    private List<Word> words;

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }
}
